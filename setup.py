from sys import version_info

from setuptools import setup

if version_info.major == 3 and version_info.minor < 6 or \
        version_info.major < 3:
    print('Your Python interpreter must be 3.6 or greater!')
    exit(1)

from saga import __version__

setup(name='saga',
      version=__version__,
      description='Stasis Result Aggregator',
      url='',
      author='Diego Pedrosa',
      author_email='dpedrosa@ucdavis.edu',
      license='GPLv3',
      packages=['saga'],
      scripts=[],
      setup_requires=['pytest-runner'],
      tests_require=['pytest', 'pytest-mock', 'pytest-cov'],
      install_requires=[
          'boto3',
          'simplejson'
      ],
      include_package_data=True,
      zip_safe=False,
      classifiers=[
          'Programming Language :: Python :: 3.6',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Intended Audience :: Science/Research'
      ])
