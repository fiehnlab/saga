import multiprocessing
from multiprocessing.pool import Pool

import requests


def query(x):
    try:
        url = '%s/result/%s' % (x[1], x[0]['id'])
        result = requests.get(url)
        if result.status_code == 200:
            print("query: %s" % result.text)
            return result.json()
        else:
            return {'id': x[0]['id'], 'sample': x[0]['sample'], 'injections': []}
    except Exception as ce:
        print("ERROR: %s " % str(ce))
        return {'id': x[0]['id'], 'sample': x[0]['sample'], 'injections': []}


class Saga:
    def __init__(self, api_url, stage):
        # setup number of cores to use
        self.agents = multiprocessing.cpu_count() // 2
        print('Using %d cores.' % self.agents)

        api_url = api_url.replace('https://', '')
        if stage == 'prod':
            self.api_url = "https://%s" % api_url
        else:
            self.api_url = ('https://%s-%s' % (stage, api_url))
        print("AWS url: %s" % api_url)

    def get_results(self, experiment):
        samp_resp = requests.get('%s/experiment/%s' % (self.api_url, experiment))

        if samp_resp.status_code == 200:
            samples = samp_resp.json()
            print("SAMPLES: %d\n%s" % (len(samples), [x['id'] for x in samples]))
            with Pool(processes=self.agents) as pool:
                results = pool.map(query, zip(samples, [self.api_url] * len(samples)))

            return results
        else:
            return {'error': 'Can\'t find that experiment'}
