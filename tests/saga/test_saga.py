import pytest

from saga.saga import Saga


@pytest.fixture
def fsaga():
    client = Saga('api.metabolomics.us/stasis', 'test')
    return client


def test_get_results(fsaga):
    experiment = 'unknown'

    result = fsaga.get_results(experiment)

    assert result
