import sys

from saga.saga import Saga

if __name__ == '__main__':
    if len(sys.argv) == 1:
        experiment = input("We need an experiment id to retrieve results from: ")
    else:
        experiment = sys.argv[1]

    print("Collecting results for experiment: %s... this may take a while" % experiment)
    saga = Saga('api.metabolomics.us/stasis', 'test')

    print(saga.get_results(experiment))
